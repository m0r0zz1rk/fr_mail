from django.contrib import admin
from .models import *


@admin.register(Mailing)
class MailingAdmin(admin.ModelAdmin):
    list_display = ('date_start', 'date_end', 'text', 'prop', 'prop_value')


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('phone', 'operators_code', 'tag', 'timezone')

