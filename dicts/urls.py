from django.urls import path
from .views import *


urlpatterns = [
    path('clients/', ClientsViewSet.as_view({'get': 'list'})),
    path('new_client/', ClientCreateViewSet.as_view({'post': 'create'})),
    path('update_client/', ClientUpdateViewSet.as_view({'get': 'retrieve', 'put': 'update'})),
    path('delete_client/', ClientDeleteViewSet.as_view({'get': 'retrieve', 'delete': 'destroy'})),
    path('mailings/', MailingViewSet.as_view({'get': 'list'})),
    path('new_mailing/', MailingCreateViewSet.as_view({'post': 'create'})),
    path('update_mailing/', MailingUpdateViewSet.as_view({'get': 'retrieve', 'put': 'update'})),
    path('delete_mailing/', MailingDeleteViewSet.as_view({'get': 'retrieve', 'delete': 'destroy'}))
]