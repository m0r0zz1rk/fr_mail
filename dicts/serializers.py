import pytz
from rest_framework import serializers
from timezone_field.rest_framework import TimeZoneSerializerField
from .models import *


class ClientSerializer(serializers.ModelSerializer):
    """Удаление клиента/Просмотр всех клиентов"""
    timezone = TimeZoneSerializerField(use_pytz=True, label='Часовой пояс')

    class Meta:
        model = Client
        fields = "__all__"


class ClientCUSerializer(serializers.ModelSerializer):
    """Добавление/обновление клиента"""
    timezone = serializers.ChoiceField(choices=pytz.all_timezones, label='Часовой пояс')

    class Meta:
        model = Client
        fields = "__all__"


class MailingSerializerWithId(serializers.ModelSerializer):
    """Класс для получения ID записи"""
    id = serializers.ReadOnlyField()


class MailingSerializer(MailingSerializerWithId):
    """Добавление/обновление/удаление рассылки"""

    date_start = serializers.DateTimeField(format='%d.%m.%Y %H:%M',
                                           label='Дата и время запуска рассылки')
    date_end = serializers.DateTimeField(format='%d.%m.%Y %H:%M',
                                         label='Дата и время окончания рассылки')

    class Meta:
        model = Mailing
        fields = "__all__"
