import datetime

from django.core.validators import RegexValidator
from django.db import models
from timezone_field import TimeZoneField

"""Кортеж значений для фильтра свойств клиентов"""
PROPERTY_CHOICES = (
    ('Code', 'Код оператора'),
    ('Tag', 'Тег')
)


class Mailing(models.Model):
    """Модель рассылок"""
    date_start = models.DateTimeField(default=datetime.datetime.now,
                                      blank=False,
                                      null=False,
                                      verbose_name='Дата и время запуска рассылки'
                                      )
    text = models.TextField(max_length=250,
                            blank=False,
                            null=False,
                            verbose_name="Текст сообщения")
    prop = models.CharField(choices=PROPERTY_CHOICES,
                            max_length=5,
                            verbose_name="Фильтр свойств клиентов")
    prop_value = models.CharField(max_length=100,
                                  verbose_name="Значение выбранного фильтра")
    date_end = models.DateTimeField(default=datetime.datetime.now,
                                    blank=False,
                                    null=False,
                                    verbose_name='Дата и время окончания рассылки'
                                    )

    def __str__(self):
        return f"Рассылка от {self.date_start.strftime('%d.%m.%Y')} " \
               f"для абонентов: свойство - {self.prop}; значение - {self.prop_value}"

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'


class Client(models.Model):
    """Модель клиентов"""
    phone_regex = RegexValidator(regex=r'^\+?7?\d{10}$',
                                 message="Телефон в формате: 7ХХХХХХХХХХ"
                                 )
    phone = models.CharField(validators=[phone_regex],
                             unique=True,
                             max_length=11,
                             blank=False,
                             verbose_name='Номер телефона клиента'
                             )
    operators_code = models.PositiveIntegerField(blank=True,
                                                 verbose_name='Код мобильного оператора'
                                                 )
    tag = models.CharField(max_length=30,
                           blank=True,
                           verbose_name='Тег(произвольная метка)'
                           )
    timezone = TimeZoneField(choices_display="WITH_GMT_OFFSET",
                             verbose_name='Часовой пояс')

    def __str__(self):
        if self.phone is None:
            return f'Клиент с тегом "{self.tag}"'
        else:
            return f'Клиент с номером телефона: {self.phone}'

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'
