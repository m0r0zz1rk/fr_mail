from django.http import Http404
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, status
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from service.tasks import MailingService
from service.views import CheckMailingTime
from .serializers import *
from .models import *


class ClientsViewSet(viewsets.ReadOnlyModelViewSet):
    """Получение списка клиентов"""
    queryset = Client.objects.all()

    def get_serializer_class(self):
        return ClientSerializer


class ClientCreateViewSet(viewsets.ModelViewSet):
    """Добавление клиента"""
    serializer_class = ClientCUSerializer

    def create(self, request, *args, **kwargs):
        client = ClientCUSerializer(data=request.POST)
        if client.is_valid(raise_exception=True):
            client.save()
            return Response('Клиент успешно добавлен')


id_client = openapi.Parameter('id_client',
                                   in_=openapi.IN_QUERY,
                                   description='ID клиента',
                                   type=openapi.TYPE_INTEGER)


class ClientUpdateViewSet(viewsets.ModelViewSet):
    """Обновление данных клиента"""
    queryset = Client.objects.all()
    serializer_class = ClientCUSerializer

    @swagger_auto_schema(manual_parameters=[id_client])
    def retrieve(self, request, pk=None):
        client = get_object_or_404(self.queryset, pk=request.GET['pk'])
        serializer = ClientCUSerializer(client)
        return Response(serializer.data)

    def update(self, request, pk=None):
        client = get_object_or_404(self.queryset, pk=request.GET['pk'])
        data = {
            'phone': request.POST['phone'],
            'operators_code': request.POST['operators_code'],
            'tag': request.POST['tag'],
            'timezone': request.POST['timezone'],
        }
        serializer = self.serializer_class(instance=client,
                                           data=data,
                                           partial=True)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response('Данные клиента успешно обновлены')


class ClientDeleteViewSet(viewsets.ModelViewSet):
    """Удаление клиента"""
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    @swagger_auto_schema(manual_parameters=[id_client])
    def retrieve(self, request, pk=None):
        client = get_object_or_404(self.queryset, pk=request.GET['pk'])
        serializer = ClientCUSerializer(client)
        return Response(serializer.data)

    @swagger_auto_schema(responses={200: 'Клиент успешно удален'})
    def destroy(self, request, pk=None):
        try:
            instance = get_object_or_404(self.queryset, pk=request.GET['pk'])
            self.perform_destroy(instance)
            return Response(data='Клиент успешно удален', status=status.HTTP_200_OK)
        except Http404:
            pass


class MailingViewSet(viewsets.ReadOnlyModelViewSet):
    """Получение списка всех рассылок"""
    queryset = Mailing.objects.all()

    def get_serializer_class(self):
        return MailingSerializer


class MailingCreateViewSet(viewsets.ModelViewSet):
    """Создание рассылки"""
    serializer_class = MailingSerializer

    def create(self, request, *args, **kwargs):
        mailing = self.serializer_class(data=request.POST)
        if mailing.is_valid(raise_exception=True):
            saved = mailing.save()
            if CheckMailingTime(saved.id) is True:
                MailingService.delay(saved.id)
            return Response(data=f'Рассыла успешно создана', status=status.HTTP_201_CREATED)


id_mailing = openapi.Parameter('id_mailing',
                                   in_=openapi.IN_QUERY,
                                   description='ID рассылки',
                                   type=openapi.TYPE_INTEGER)


class MailingUpdateViewSet(viewsets.ModelViewSet):
    """Обновление атрибутов рассылки"""
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    @swagger_auto_schema(manual_parameters=[id_mailing])
    def retrieve(self, request, pk=None):
        mailing = get_object_or_404(self.queryset, pk=request.GET['pk'])
        data = {
            'date_start': mailing.date_start.strftime('%d.%m.%Y %H:%M'),
            'date_end': mailing.date_end.strftime('%d.%m.%Y %H:%M'),
            'text': mailing.text,
            'prop': mailing.prop,
            'prop_value': mailing.prop_value,
        }
        serializer = self.serializer_class(instance=mailing,
                                           data=data,
                                           partial=True)
        if serializer.is_valid():
            pass
        return Response(serializer.data)

    @swagger_auto_schema(responses={200: 'Данные рассылки успешно обновлены'})
    def update(self, request, pk=None):
        mailing = get_object_or_404(self.queryset, pk=request.GET['pk'])
        serializer = self.serializer_class(instance=mailing,
                                           data=request.POST,
                                           partial=True)
        if serializer.is_valid(raise_exception=True):
            saved = serializer.save()
            if CheckMailingTime(saved.id) is True:
                MailingService.delay(saved.id)
            return Response(data='Данные рассылки успешно обновлены', status=status.HTTP_200_OK)


class MailingDeleteViewSet(viewsets.ModelViewSet):
    """Удаление рассылки"""
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    @swagger_auto_schema(manual_parameters=[id_mailing])
    def retrieve(self, request, pk=None):
        mailing = get_object_or_404(self.queryset, pk=request.GET['pk'])
        serializer = self.serializer_class(mailing)
        return Response(serializer.data)

    @swagger_auto_schema(responses={200: 'Рассылка успешно удалена'})
    def destroy(self, request, pk=None):
        try:
            mailing = get_object_or_404(self.queryset, pk=request.GET['pk'])
            self.perform_destroy(mailing)
            return Response(data='Рассылка успешно удалена', status=status.HTTP_200_OK)
        except Http404:
            pass
