### Сервис рассылки сообщений
___

#### Назначение

Выполнение тестового задания создания API сервиса
рассылки сообщений клиентам и сбора статистики
___

#### Зависимости
В проекте использованы следующие пакеты:
1. Django Rest Framework;
2. Django Timezone Field;
3. Django-environ;
4. Yet Another Swagger Generator (drf-yasg);
5. Celery (в качестве брокера Redis);
6. Requests   
Полный список представлен в файле `req.txt` в корне проекта

___
#### Запуск проекта
Для запуска проекта в файле docker-compose указан файл env, в корне директории `config` необходимо создать файл `.env`, в котором написать следующие строки:  
`REDIS_HOST={IP}`, где вместо `{IP}`указать IP адрес Redis (если установлен из зависимостей, то запустить сервер локально и указать адрес: `127.0.0.1`)  
`REDIS_PORT={PORT}`, где вместо `{PORT}` указать порт, на котором работает Redis  
`SECRET_KEY={KEY}`, где вместо `{KEY}` указать секретный ключ для Django проекта (можно сгенерировать [здесь](https://djecrety.ir/))  
`JWT={TOKEN}`, где вместо `{TOKEN}` указать JWT токен для авторизации в сервисе рассылки  

___

#### Приложения
+ `dicts` - приложение для работы со словарями: рассылки, клиенты
+ `service` - приложение для работы с сервисом отправки сообщения
+ `stats` - приложения для работы со статистикой по рассылкам
___

#### API
Приложение `dicts` содержит следующие эндпоинты:  
`GET /dicts/clients` - получение списка всех клиентов;  
`POST /dicts/new_client` - добавление нового клиента;  
`GET,PUT /dicts/update_client` - обновление данных клиента по ID;  
`GET,DELETE /dicts/delete_client` - удаление клиента по ID;  
`GET /dicts/mailings` - получение списка всех рассылок;  
`POST /dicts/new_mailing` - добавление новой рассылки;  
`GET,PUT /dicts/update_mailing` - обновление данных рассылки по ID;  
`GET,DELETE /dicts/delete_mailing` - удаление рассылки по ID;  
  
Приложение `stats` содержит следующие эндпоинты:  
`GET /stats/new_stat/` - формирование нового среза общей статистики по всем рассылкам;  
`GET /stats/gen_stat` - получение списка всех срезов общей статистики по всем рассылкам;  
`GET /stats/det_stat` - получение детальной статистики рассылки по ее ID.  
  
OpenAPI спецификация расположена по URN `/docs/` (доп задание под номером 5)
