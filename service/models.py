from django.db import models
from dicts.models import Mailing, Client


class Message(models.Model):
    """Модель сообщений"""
    date_create = models.DateTimeField(auto_now_add=True,
                                       verbose_name='Дата и время создания (отправки)'
                                       )
    status = models.CharField(max_length=30,
                              verbose_name='Статус отправки')
    mailing = models.ForeignKey(
        Mailing,
        on_delete=models.CASCADE,
        null=False,
        default='',
        verbose_name='Рассылка'
    )
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        null=False,
        default='',
        verbose_name='Клиент'
    )

    def __str__(self):
        return f'Сообщение от {self.date_create} из рассылки: {self.mailing}'

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
