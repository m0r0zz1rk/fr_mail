import datetime

from config.celery import app
from dicts.models import Mailing, Client
from .models import Message
from .service import ProbeMail


@app.task
def MailingService(id_mailing):
    """Выборка клиентов по указанному в рассылке свойству и
    вызов фукнции отправки сообщение через сервис отправки Probe"""
    if Mailing.objects.filter(id=id_mailing).exists():
        mailing = Mailing.objects.get(id=id_mailing)
        cls = None
        if mailing.prop == 'Code':
            if Client.objects.filter(operators_code=mailing.prop_value).exists():
                cls = Client.objects.filter(operators_code=mailing.prop_value)
        else:
            if Client.objects.filter(tag=mailing.prop_value).exists():
                cls = Client.objects.filter(tag=mailing.prop_value)
        if cls is not None:
            for cl in cls:
                if datetime.datetime.now() <= mailing.date_end.replace(tzinfo=None):
                    if not Message.objects.filter(mailing_id=id_mailing).filter(client_id=cl.id).exists():
                        ProbeMail(id_mailing, cl.id)
                else:
                    break
        else:
            return 'cls is empty'
    else:
        return 'Mailing not found'


@app.task
def CheckMailingDateTime():
    """Задача по расписанию: ищет необходимые к выполнению рассылки, если
    текущее время попадает в интервал между стартом и окончанием - выполнить"""
    if Mailing.objects.filter(date_start__lte=datetime.datetime.now()).\
            filter(date_end__gte=datetime.datetime.now()).exists():
        for m in Mailing.objects.filter(date_start__lte=datetime.datetime.now()).\
                filter(date_end__gte=datetime.datetime.now()):
            MailingService(m.id)
