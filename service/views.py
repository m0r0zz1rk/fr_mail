import datetime
from dicts.models import Mailing


def CheckMailingTime(id_mailing):
    """Проверка даты и времени запуска рассылки"""
    if Mailing.objects.filter(id=id_mailing).exists():
        mailing = Mailing.objects.get(id=id_mailing)
        if mailing.date_start.replace(tzinfo=None) <= datetime.datetime.now().replace(tzinfo=None):
            if datetime.datetime.now().replace(tzinfo=None) <= mailing.date_end.replace(tzinfo=None):
                return True
            else:
                return False
        else:
            return False
    else:
        return False

