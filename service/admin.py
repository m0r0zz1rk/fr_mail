from django.contrib import admin

from service.models import Message


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('date_create', 'status', 'client')
