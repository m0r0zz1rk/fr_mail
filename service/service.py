import datetime
import os
import requests

from config import settings
from dicts.models import Mailing, Client
from stats.models import DetailStatistic
from .models import Message


def ProbeMail(id_mailing, id_client):
    """Отправка сообщения через сервис Probe"""
    if Mailing.objects.filter(id=id_mailing).exists() and \
        Client.objects.filter(id=id_client).exists():
        mailing = Mailing.objects.get(id=id_mailing)
        client = Client.objects.get(id=id_client)
        new_message = Message()
        new_message.status = 'Подготовлено к отправке'
        new_message.mailing_id = id_mailing
        new_message.client_id = id_client
        new_message.save()
        id_message = new_message.id
        endpoint = f'https://probe.fbrq.cloud/v1/send/{id_message}'
        head = {
            'accept': 'application/json',
            'Authorization': f"Bearer {settings.JWT_TOKEN}",
            'Content-Type': 'application/json',
        }
        data = {
            'id': id_message,
            'phone': int(client.phone),
            'text': mailing.text,
        }
        t_send = datetime.datetime.now()
        response = requests.post(endpoint,
                                verify=os.path.join(settings.BASE_DIR, 'consolidate.pem'),
                                json=data,
                                headers=head,
                                timeout=30)
        try:
            message = Message.objects.get(id=id_message)
            message.status = 'Передано в сервис отправки'
            message.save()
            detail_stat = DetailStatistic()
            detail_stat.mailing_id = id_mailing
            detail_stat.client_id = id_client
            detail_stat.time_send = t_send
            if response.status_code == 200:
                message.status = 'Отправлено'
                detail_stat.status = 'Отправлено'
            else:
                message.status = f'Ошибка от сервиса (Код:{response.status_code})'
                detail_stat.status = f'Ошибка от сервиса (Код:{response.status_code})'
            message.save()
            detail_stat.save()
        except BaseException:
            raise LookupError('Ошибка при обращении к сообщению')