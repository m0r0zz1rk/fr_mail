from dicts.models import Mailing
from service.models import Message
from .models import GeneralStatistic


def NewGenStat():
    """Формирование нового среза общей статистики"""
    try:
        new_stat = GeneralStatistic()
        new_stat.total_mailing = Mailing.objects.count()
        new_stat.total_messages = Message.objects.count()
        new_stat.total_success = Message.objects.filter(status__contains='Отправлено').count()
        new_stat.total_error = Message.objects.exclude(status__contains='Отправлено').count()
        new_stat.save()
        return 'OK'
    except BaseException:
        return 'ERROR'