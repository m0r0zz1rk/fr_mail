from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from stats.models import GeneralStatistic, DetailStatistic


class GeneralStatisticSerializer(ModelSerializer):
    """Сериализатор модели общей статистики"""
    class Meta:
        model = GeneralStatistic
        fields = '__all__'


class DetailStatisticSerializer(ModelSerializer):
    """Сериализатор модели детальной статистики"""
    client = serializers.SlugRelatedField(slug_field='phone', read_only=True)
    time_send = serializers.DateTimeField(format='%d.%m.%Y %H:%M',
                              label='Дата и время запуска рассылки')

    class Meta:
        model = DetailStatistic
        fields = ('client', 'time_send', 'status')
