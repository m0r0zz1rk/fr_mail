from django.contrib import admin

from .models import GeneralStatistic, DetailStatistic


@admin.register(GeneralStatistic)
class GeneralStatisticAdmin(admin.ModelAdmin):
    list_display = [field.name for field in GeneralStatistic._meta.get_fields()]


@admin.register(DetailStatistic)
class DetailStatisticAdmin(admin.ModelAdmin):
    list_display = [field.name for field in DetailStatistic._meta.get_fields()]
