import datetime

from django.db import models

from dicts.models import Mailing, Client


class GeneralStatistic(models.Model):
    """Модель общей статистики"""
    date_create = models.DateField(
        auto_now_add=True,
        verbose_name='Дата запроса статистики'
    )
    total_mailing = models.PositiveIntegerField(
        verbose_name='Количетсво рассылок'
    )
    total_messages = models.PositiveIntegerField(
        verbose_name='Общее количество сообщений'
    )
    total_success = models.PositiveIntegerField(
        verbose_name='Общее количество отправленных сообщений'
    )
    total_error = models.PositiveIntegerField(
        verbose_name='Общее количество неотправленных сообщений'
    )

    def __str__(self):
        return f"Общая статистика от {self.date_create.strftime('%d.%m.%Y')}"

    class Meta:
        verbose_name = 'Общая статистика'
        verbose_name_plural = 'Общая статистика'


class DetailStatistic(models.Model):
    """Модель детальной статистики по рассылке"""
    mailing = models.ForeignKey(
        Mailing,
        on_delete=models.PROTECT,
        default='',
        null=False,
        related_name='DetStatMailing',
        verbose_name='Рассылка'
    )
    client = models.ForeignKey(
        Client,
        on_delete=models.PROTECT,
        default='',
        null=False,
        related_name='DetStatClient',
        verbose_name='Клиент'
    )
    time_send = models.DateTimeField(
        default=datetime.datetime.now,
        verbose_name='Время отправки сообщения'
    )
    status = models.CharField(
        max_length=30,
        verbose_name='Статус сообщения'
    )

    def __str__(self):
        return f'Детальная статистика по рассылке {str(self.mailing.id)}'

    class Meta:
        verbose_name = 'Детальная статистика'
        verbose_name_plural = 'Детальные статистики'
