from django.http import JsonResponse
from django.utils.decorators import method_decorator
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import api_view, action
from rest_framework.viewsets import ReadOnlyModelViewSet, ModelViewSet

from dicts.models import Mailing
from dicts.views import id_mailing
from service.models import Message
from .models import GeneralStatistic, DetailStatistic
from .serializers import GeneralStatisticSerializer, DetailStatisticSerializer
from .service import NewGenStat


class GeneralStatisticListViewSet(ReadOnlyModelViewSet):
    """Список срезов общей статистики по рассылкам"""
    queryset = GeneralStatistic.objects.all()

    def get_serializer_class(self):
        return GeneralStatisticSerializer


@swagger_auto_schema(method='get',
                     responses={200: 'Срез успешно сформирован'})
@api_view(['GET'])
def GeneralStatisticNew(request):
    """Сформировать новый срез общей статистики"""
    result = NewGenStat()
    return JsonResponse({'result': result})


class DetailStatisticViewSet(ModelViewSet):
    """Детальная статистика по конкретной рассылке"""
    serializer_class = DetailStatisticSerializer

    def get_queryset(self):
        try:
            id_mailing = self.request.GET.get('id')
        except ValueError:
            return None
        return DetailStatistic.objects.filter(mailing_id=id_mailing)

    @swagger_auto_schema(manual_parameters=[id_mailing])
    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.serializer_class(queryset)
        return JsonResponse(serializer.data)


