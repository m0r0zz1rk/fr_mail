from django.urls import path
from .views import GeneralStatisticListViewSet, GeneralStatisticNew, DetailStatisticViewSet

urlpatterns = [
    path('gen_stat/', GeneralStatisticListViewSet.as_view({'get': 'list'})),
    path('det_stat/', DetailStatisticViewSet.as_view({'get': 'list'})),
    path('new_stat/', GeneralStatisticNew),
]